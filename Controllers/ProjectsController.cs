using Microsoft.AspNetCore.Mvc;
using TaskManagerAPI.Models;
using TaskManagerAPI.Services;

namespace TaskManagerAPI.Controllers
{
    [Route("api/Projects")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly TaskManagerDbContext db;
        private readonly IUsersService userService;
        public ProjectsController(TaskManagerDbContext _db, IUsersService _usersService)
        {
            db = _db;
            userService = _usersService;
        }

        [HttpGet]
        public List<Project> Get()
        {

            List<Project> projects = db.Projects.ToList();
            return projects;

        }


        [HttpPost]
        public Project Post([FromBody] Project project)
        {
            db.Projects.Add(project);
            db.SaveChanges();
            return project;

        }

        [HttpPut]
        public Project Put([FromBody] Project project)
        {
            var existingProject = db.Projects.Where(temp => temp.ProjectID == project.ProjectID).FirstOrDefault();

            if (existingProject != null)
            {
                existingProject.ProjectName = project.ProjectName;
                existingProject.DateOfStart = project.DateOfStart;
                existingProject.TeamSize = project.TeamSize;

                db.Update(existingProject);
                db.SaveChanges();
                return existingProject;
            }
            else
            {
                return null;
            }

        }

        [HttpDelete]
        public int Delete(int projectID)
        {
            var existingProject = db.Projects.Where(temp => temp.ProjectID == projectID).FirstOrDefault();

            if (existingProject != null)
            {
                db.Projects.Remove(existingProject);
                db.SaveChanges();
                return projectID;
            }
            else
            {
                return -1;
            }

        }

        //http://localhost:5271/api/Projects/search/ProjectID/9
        [HttpGet]
        [Route("search/{searchby}/{searchtext}")]
        public List<Project> Search(string searchBy, string searchText)
        {
            List<Project> projects = null;
            if (searchBy == "ProjectID")
                projects = db.Projects.Where(temp => temp.ProjectID.ToString().Contains(searchText)).ToList();
            else if (searchBy == "ProjectName")
                projects = db.Projects.Where(temp => temp.ProjectName.Contains(searchText)).ToList();
            if (searchBy == "DateOfStart")
                projects = db.Projects.Where(temp => temp.DateOfStart.ToString().Contains(searchText)).ToList();
            if (searchBy == "TeamSize")
                projects = db.Projects.Where(temp => temp.TeamSize.ToString().Contains(searchText)).ToList();

            return projects;
        }

        [HttpPost]
        [Route("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] LoginDTO loginDTO)
        {
            //var user = await userService.Authenticate(loginDTO);
            var user = db.Users.Where(el => el.UserName == loginDTO.Username && el.PasswordHash == loginDTO.Password);//FindAsync<ApplicationUser>("1");
            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });
            return Ok(user);
        }


    }
}