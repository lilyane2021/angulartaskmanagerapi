using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using TaskManagerAPI.Models;

namespace TaskManagerAPI.Services
{
    public class UsersService : IUsersService
    {
        private readonly UserManager<ApplicationUser> _applicationUserManager;
        private readonly SignInManager<ApplicationUser> _applicationSignInManager;

        public UsersService(UserManager<ApplicationUser> applicationUserManager, SignInManager<ApplicationUser> applicationSignInManager)
        {
            this._applicationUserManager = applicationUserManager;
            this._applicationSignInManager = applicationSignInManager;
            // this._appSettings = appSettings.Value;
        }

        public async Task<ApplicationUser> Authenticate(LoginDTO loginDTO)
        {
            var result = await _applicationSignInManager.PasswordSignInAsync(loginDTO.Username, loginDTO.Password, false, false);
            if (result.Succeeded)
            {
                var applicationUser = await _applicationUserManager.FindByNameAsync(loginDTO.Username);
                applicationUser.PasswordHash = null;

                // var tokenHandler = new JwtSecurityTokenHandler();
                // var key = System.Text.Encoding.ASCII.GetBytes("HERE IS THE SECRET");
                // var tokenDescriptor = new Microsoft.IdentityModel.Tokens.SecurityTokenDescriptor()
                // {
                //     Subject = new ClaimsIdentity(new Claim[] {
                //         new Claim(ClaimTypes.Name, applicationUser.Id),
                //         new Claim(ClaimTypes.Email, applicationUser.Email)
                //     }),
                //     Expires = DateTime.UtcNow.AddHours(8),
                //     SigningCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(key), Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature)
                // };
                // var token = tokenHandler.CreateToken(tokenDescriptor);
                // applicationUser.Token = tokenHandler.WriteToken(token);

                return applicationUser;
            }
            else
            {
                return null;
            }
        }
    }
}