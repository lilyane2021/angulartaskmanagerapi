using TaskManagerAPI.Models;

namespace TaskManagerAPI.Services
{
    public interface IUsersService
    {
        Task<ApplicationUser> Authenticate(LoginDTO loginDTO);
    }
}