using Microsoft.AspNetCore.Identity;

namespace TaskManagerAPI.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string? Name { get; set; }
        // public string? Token { get; set; }

    }
}